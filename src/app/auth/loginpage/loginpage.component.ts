import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from "@angular/router";
import { ApiServiceService } from 'src/app/api-service.service';
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
  clientId ="219531800490-pfod758iaikjfphmce5v7ltioqpi846r.apps.googleusercontent.com";
  constructor(private router: Router,private apiService:ApiServiceService,private authService: SocialAuthService) { }

  ngOnInit(): void {
  }


  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res)=>{
      this.isLoading = true;
     this.apiService.postMethod('/admin/googleLogin',res).subscribe((resp:any)=>{
       if( resp.status ==200){
        localStorage.setItem("USER",JSON.stringify(resp));
        this.isLoading = false;
        this.router.navigate(['/dashboard/dashbaord_page']);
       }
     },(err)=>{
      this.isLoading = false
     });
    });
  }

  gotopage(){
    this.router.navigate(['dashboard/indexpage'])
  }

  gopage(){
    this.router.navigate(['auth/forgot_page'])
  }
  goregisterpage(){
    this.router.navigate(['auth/register_page'])
  }
  email:any;
  password:any;
  isLoading:boolean;
  isError:boolean;
  errorMsg:any;

  login(){
    if(this.email && this.password){
      this.isLoading = true
      let data={
        email:this.email,
        password:this.password
      }
     this.apiService.postMethod('/admin/userlogin',data).subscribe((response:any)=>{
       if(response.status == 200){
         localStorage.setItem("USER",JSON.stringify(response));
        this.isLoading = false
        // this.toastr.success("","Login Successfully")
        this.router.navigate(['/dashboard/dashbaord_page']);
       } else {
        this.isLoading = false;
        this.isError =true;
        this.errorMsg= response.data
        // this.toastr.error("","Invalid email and password")
       }
     })
    } else {
      this.isLoading = false;
      this.isError =true;
      this.errorMsg= "Please enter email and password"
      // this.toastr.error("","Please enter email and password")
    }
  }

}
