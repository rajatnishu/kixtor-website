import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api-service.service';
import { countryList } from '../../countryList';
import { CodeList } from './../../countyCode'

@Component({
  selector: 'app-registerpage',
  templateUrl: './registerpage.component.html',
  styleUrls: ['./registerpage.component.css']
})
export class RegisterpageComponent implements OnInit {
  step: any = "1";
  name: any;
  email: any;
  contactNumber: any;
  gender: any;
  password: any;
  rePassword: any;
  country: any = null;
  countryList: any = countryList;
  state: any = null;
  strengths: any = null;
  profilePic: any;
  skills: any = null;
  stage: any;
  stateList: any = [];
  stepFirstMandatory = ["name", "contactNumber", "email", "gender", "state", "country"]
  setSecondMandatory = ["strengths", "skills", "stage"];
  errorMessage: any;
  isEdit: boolean = false;
  issuccess:boolean=false;
  id: any;
  countryCode: any = null;
  codeList = CodeList;
  editImage: boolean = false;
  isError = false;
  errorMsg= "";
  stageList = [
    { name: "Ideation", value: false },
    { name: "I have a Business Plan", value: false },
    { name: "Pilot Testing OR MVP", value: false },
    { name: "I want to scale up", value: false }
  ];
  industryList = [
    { name: "3D Printing", value: false },
    { name: "Artificial Intelligence", value: false },
    { name: "Machine Learning", value: false },
    { name: "Legal", value: false },
    { name: "Design", value: false },
    { name: "Substainability", value: false },
    { name: "Supply Chain", value: false },
    { name: "Logistics", value: false },
    { name: "Green Initiatives", value: false },
    { name: "MLM / Network Marketing", value: false },
    { name: "Consumer Products", value: false },
    { name: "Finance", value: false },
    { name: "Social", value: false },
    { name: "Real Estate", value: false },
    { name: "Biotech", value: false },
    { name: "Events", value: false },
    { name: "Fitness", value: false },
    { name: "Tourism", value: false },
    { name: "Hospitality", value: false },
    { name: "Technology", value: false },
    { name: "Consulting", value: false },
    { name: "Ecommerce", value: false },
    { name: "Fashion", value: false },
    { name: "Healthcare", value: false },
    { name: "Recruitments", value: false },
    { name: "Critical Infrastucture", value: false },
    { name: "Security", value: false },
    { name: "Agriculture", value: false },
    { name: "Education", value: false },
    { name: "Others", value: false }
  ]
  allStrengths: any;
  allSkills: any;
  isLoading: boolean =false;
  addprofile: any;
  Uploaded_filename: any;
  image = true;
  constructor(private apiservice: ApiServiceService,private router :Router) { }

  async ngOnInit() {
     this.getAllStrengths();
     this.getAllSkills();
   }

   getAllStrengths(){
    this.apiservice.getMethod('/admin/allcategory').subscribe((data: any) => {
      this.allStrengths = data.data
    })
  }
  getAllSkills(){
    this.apiservice.getMethod('/admin/allskill').subscribe((data: any) => {
      this.allSkills = data.data
    })
  }

  selectIndustry(event,i){
    this.industryList[i].value = event.target.checked; 
  }

  selectStrength(event, i) {
    this.stageList[i].value = event.target.checked;
  }

  
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  choosePhoto(element) {
    const self = this;
    var uploadImg = element.target.files[0]
    var FileSize = element.target.files[0].size / 1024 / 1024;
    if (FileSize > 2) {
      this.errorMessage ='File size exceeds 2 MB'
      this.isError =true
      return
    }
    var fileName = element.target.files[0].name;
      self.Uploaded_filename = fileName
      const reader = new FileReader();
      reader.readAsDataURL(uploadImg);
      reader.onload = () => {
        console.log(reader.result);
        this.image = false
        this.addprofile = reader.result;
        this.editImage =false;
      };
  }

  terms : Boolean =  false
  continue(value) {
    let checkMandatory = false;
    this.isError =false;
    debugger
    if(value== '2'){
      if(this.terms){
        for(let i=0;i<this.stepFirstMandatory.length;i++){
          if(!this[this.stepFirstMandatory[i]]){
            checkMandatory= true;
          }
        }
        if(checkMandatory){
          this.errorMessage ='Please fill all mandatory field'
          this.isError =true
          return
          // return this.toastr.error("","Please fill all mandatory field ");
        } else {
          if(this.contactNumber && this.contactNumber.length < 10){
            // return this.toastr.error("","Contact number should be 10 digit");
          }
          if(this.email){
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            let checkMail = re.test(this.email);
            if(!checkMail){
              this.errorMessage ='Please enter valid email'
              this.isError =true
              return
              // return this.toastr.error("","Please enter valid email");
            }
          }
  
          if(this.password){
             if(!this.rePassword){
              this.errorMessage ='Please enter retype password'
              this.isError =true
              return
                // return this.toastr.error("","Please enter retype password");              
             }
             if(this.password !== this.rePassword){
              this.errorMessage ='Password and Retype Password not match'
              this.isError =true
              return
                // return this.toastr.error("","Password and Retype Password not match");
             }
          }
          this.step = value;
        }
      } else{
        this.errorMessage ='first read Terms and Conditions'
        this.isError =true
      }
    } else if(value== '3'){
      if(!this.Uploaded_filename){
          this.errorMessage ='Please select image'
          this.isError =true
          return
        // return this.toastr.error("","Please select image");
      }
       if(!this.strengths){
        this.errorMessage ='Please Select Strengths'
        this.isError =true
        return
        // return this.toastr.error("","Please Select Strengths");
       }
       let findStage = this.stageList.find((d)=> d.value == true)
       if(!findStage){
        this.errorMessage ='Please Select Stage'
        this.isError =true
        return
       }
       if(!this.skills){
        this.errorMessage ='Please Select Skill'
        this.isError =true
        return
        // return this.toastr.error("","Please Select Stage");
       }
       this.step = value;
      }
    // this.step = value;
  }


  async selectCountry(value){
    this.state= null;
    let data={
      code:value.code
    }
    await this.apiservice.postMethod('/getstate' ,data).subscribe((res)=>{
      this.stateList = res;
    })
  }

  gotopage(){
    this.router.navigateByUrl('/auth/login_page');
  }

  gopage(){
    this.router.navigateByUrl('/dashboard/terms_page');
  }

  submit() {
    this.isError=false;
    let industry = [];
    for (let i = 0; i < this.industryList.length; i++) {
      if (this.industryList[i].value) {
        industry.push(this.industryList[i].name);
      }
    }
    if (industry && industry.length == 0) {
      this.errorMessage ='Please Select Industry';
      this.isError=true;
      return    
    }
    let data: any = {
      name: this.name,
      contact_number: this.contactNumber,
      email: this.email,
      password: this.password,
      country: this.country.name,
      state: this.state,
      gender: this.gender,
      addprofile: this.addprofile || '',
      profile_photo: this.Uploaded_filename,
      Uploaded_filename: this.Uploaded_filename,
      strengths: this.strengths,
      stage: this.stageList,
      skills: this.skills,
      industry: industry,
      country_code: this.countryCode
    }
    if (!this.isEdit) {
      this.isLoading = true;
      this.apiservice.postMethod('/adduser', data).subscribe((data: any) => {
        if (data.status == 200) {
          this.errorMessage=data.text
          this.isLoading = false;
          this.issuccess =true;
          var self= this;
          let userid ={
            user_id : data.data._id
          }
          debugger
          this.apiservice.postMethod('/admin/addnotifaction', userid).subscribe((res : any)=>{
            console.log(res)
          })
          setTimeout(() => {
             self.router.navigate(['/dashboard/indexpage']);
          }, 500);
          // this.toastr.success('', 'User added successfully');
          // this.router.navigateByUrl('/dashbord/manage-user');
        } else {
          this.errorMessage="This email already exist"
          this.isError =true;
          this.isLoading = false;
          // this.toastr.error('', 'this email already exit');
        }
      })
    }
  }
}
