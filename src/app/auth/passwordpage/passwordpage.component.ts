import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from "@angular/router";
import { ApiServiceService } from 'src/app/api-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-passwordpage',
  templateUrl: './passwordpage.component.html',
  styleUrls: ['./passwordpage.component.css']
})
export class PasswordpageComponent implements OnInit {

  constructor(private router: Router,private apiService:ApiServiceService,private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  gotopage(){
    this.router.navigate(['dashboard/indexpage'])
  }
  gopage(){
    this.router.navigate(['auth/login_page'])
  }
  goregisterpage(){
    this.router.navigate(['auth/register_page'])
  }

  email : any
  isLoading :Boolean = false
  shownewinputbox : Boolean = true
  requsetnewpage() {
    debugger
    if (this.email) {
      this.isLoading =  true
      let data = {
        email: this.email
      }
      this.apiService.postMethod('/admin/requestemail', data).subscribe((response: any) => {
        if (response.status == 200) {
          this.isLoading =  false
          this.shownewinputbox = false
          this.toastr.success("", "Your otp send on your mail you can create your new password")
        } else {
          this.isLoading =  false
          this.toastr.error("", "please enter valid email!")
        }
      })
    } else {
      this.toastr.error("", "enter email id")
    }
  }


  otp:any
  newpassword :any
  conformpassword :any
  getconformpassword(){
    if(this.otp && this.newpassword && this.conformpassword){
      if(this.newpassword ==  this.conformpassword){
        this.isLoading =  true
        let data={
         otp: this.otp,
         email: this.email,
        newpassword : this.newpassword 
        }
        this.apiService.postMethod('/admin/updatepassword', data).subscribe((response: any) => {
          if (response.status == 200) {
            this.isLoading =  false
            debugger
            localStorage.setItem("USER",JSON.stringify(response));
             this.toastr.success('',"your password update succesfully")
             this.router.navigate(['/dashboard/dashbaord_page'])
          } else{
            this.isLoading =  false
            this.toastr.error('',"please enter currect otp")
          }
        })
      } else{
        this.toastr.error("", "both password should be match")
      }
    } else{
      this.toastr.error("", "all feild required")
    }
  }

}
