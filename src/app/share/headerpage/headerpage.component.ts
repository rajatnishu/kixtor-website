import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from "@angular/router";

@Component({
  selector: 'app-headerpage',
  templateUrl: './headerpage.component.html',
  styleUrls: ['./headerpage.component.css']
})
export class HeaderpageComponent implements OnInit {
  isLogin:boolean=false;

  constructor(private router: Router) { }

  ngOnInit(): void {
     let data = JSON.parse(localStorage.getItem("USER"));
     if(data){
      this.isLogin = true;
     } else {
       this.isLogin =false;
     }

  }
gotoHome(){
  this.router.navigate(['dashboard/dashbaord_page']);
}
  gotopage(data){
    this.router.navigate(['/auth/' + data])
  }

  mentorpage(value){
    this.router.navigate(['/dashboard/' + value])
  }
}
