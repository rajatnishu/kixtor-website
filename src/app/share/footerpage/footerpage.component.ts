import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from "@angular/router";

@Component({
  selector: 'app-footerpage',
  templateUrl: './footerpage.component.html',
  styleUrls: ['./footerpage.component.css']
})
export class FooterpageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }


  mentorpage(value){
    this.router.navigate(['/dashboard/' + value])
  }
  
}
