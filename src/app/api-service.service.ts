import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
     apiUrl = "http://localhost:3000";
    //apiUrl= "https://kxito-backend.herokuapp.com"
    constructor(private http: HttpClient) { }
  
    getMethod(apiname) {
      return this.http.get(this.apiUrl + apiname);
    }
  
    postMethod(apiname,data){
      return this.http.post(this.apiUrl + apiname, data);
    }
  
    getCountry(){
      return this.http.get('http://battuta.medunes.net/api/country/all/?key=b13bd4f6a1a938f79f6f15519d4a518f');
    }
  
    getState(code){
      return this.http.get('http://battuta.medunes.net/api/region/'+code+'/all/?key=b13bd4f6a1a938f79f6f15519d4a518f');
    }
}
