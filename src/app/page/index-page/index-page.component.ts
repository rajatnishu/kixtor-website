import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../api-service.service';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.css']
})
export class IndexPageComponent implements OnInit {

  constructor(private apiservice:ApiServiceService) { }

  ngOnInit(): void {
    this.getallmentor()
  }


  getmentor:any =[]
  getallmentor() {
    this.apiservice.getMethod('/admin/allmentor').subscribe((data: any) => {
      if (data.status == 200) {
        this.getmentor = data.result
        debugger
      }
    })
  }

}
