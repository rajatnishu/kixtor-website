import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-editprofilepage',
  templateUrl: './editprofilepage.component.html',
  styleUrls: ['./editprofilepage.component.css']
})
export class EditprofilepageComponent implements OnInit {

  constructor(private router :Router,private apiService:ApiServiceService) { }
  loginData :any ={}
  ngOnInit(): void {
    let data = localStorage.getItem("USER");
    if (data) {
      this.loginData = JSON.parse(data).data
      this.userdetalies()
    }
  }

  getIndustry(ind){
    debugger
return ind.industry.toString()
  }
  userdetalies(){
    let data={
      email: this.loginData._id,
      
    }
   this.apiService.postMethod('/admin/userdata',data).subscribe((response:any)=>{
         if(response.status == 200){
          this.loginData = response.data
          debugger
         }
   })
  }

  gotopage(){
    this.router.navigate(['dashboard/dashbaord_page']);
  }
}
