import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';
@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {

  constructor(private apiService:ApiServiceService) { }
  terms : any

  ngOnInit(): void {

    let data = {
      type: 'Privacy'
    }
    this.apiService.postMethod('/admin/allcms',data).subscribe((response:any)=>{
      this.terms = response.data.content;
    })
  }

}
