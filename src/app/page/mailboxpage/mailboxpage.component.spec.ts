import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailboxpageComponent } from './mailboxpage.component';

describe('MailboxpageComponent', () => {
  let component: MailboxpageComponent;
  let fixture: ComponentFixture<MailboxpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailboxpageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailboxpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
