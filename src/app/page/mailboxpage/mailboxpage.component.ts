import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mailboxpage',
  templateUrl: './mailboxpage.component.html',
  styleUrls: ['./mailboxpage.component.css']
})
export class MailboxpageComponent implements OnInit {
  requestType:any = 'accept'
  constructor() { }

  ngOnInit(): void {
  }


  changeRequest(value){
    this.requestType = value;
  }
}
