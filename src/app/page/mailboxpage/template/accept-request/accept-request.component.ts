import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-accept-request',
  templateUrl: './accept-request.component.html',
  styleUrls: ['./accept-request.component.css']
})
export class AcceptRequestComponent implements OnInit {
  loginData:any;
  acceptData:any=[];
  isLoading:boolean=false;
  noDataFound:boolean =false;
  constructor(private apiservice:ApiServiceService) { }

  ngOnInit(): void {
    let data = localStorage.getItem("USER");
    if (data) {
      this.loginData = JSON.parse(data).data
    }
   this.getAllSendRequest();
  }

  getAllSendRequest(){
    this.isLoading =true;
    this.noDataFound=false;
    this.apiservice.getMethod('/admin/allacceptrequest/'+this.loginData._id).subscribe((res:any)=>{
       this.acceptData = res.data;
       if( this.acceptData &&  this.acceptData.length == 0){
        this.noDataFound =true;
       }
       this.isLoading =false;
    },(err)=>{
      this.isLoading =false;
    })
  }



}
