import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-pending-request',
  templateUrl: './pending-request.component.html',
  styleUrls: ['./pending-request.component.css']
})
export class PendingRequestComponent implements OnInit {

  loginData: any;
  pendingData: any = [];
  isLoading:boolean =false;
  noDataFound:boolean=false;

  constructor(private apiservice: ApiServiceService) { }

  ngOnInit(): void {
    let data = localStorage.getItem("USER");
    if (data) {
      this.loginData = JSON.parse(data).data
    }
    this.getAllSendRequest();
  }

  getAllSendRequest() {
    this.isLoading = true;
    this.noDataFound =false;
    this.apiservice.getMethod('/admin/allpendingrequest/' + this.loginData._id).subscribe((res: any) => {
      this.pendingData = res.data;
      if( this.pendingData &&  this.pendingData.length == 0){
        this.noDataFound =true;
       }
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
    })
  }

  acceptRequest(id) {
    this.apiservice.getMethod('/admin/acceptrequest/' + id).subscribe((res: any) => {
      let resspo = res.data
    }, (err) => {
    })
  }

}
