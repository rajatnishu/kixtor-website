import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-sent-request',
  templateUrl: './sent-request.component.html',
  styleUrls: ['./sent-request.component.css']
})
export class SentRequestComponent implements OnInit {
   loginData:any;
   sendData:any=[];
   isLoading:boolean =false;
   noDataFound:boolean=false;
  constructor(private apiservice:ApiServiceService) { }

  ngOnInit(): void {
    let data = localStorage.getItem("USER");
    if (data) {
      this.loginData = JSON.parse(data).data
    }
   this.getAllSendRequest();
  }

  getAllSendRequest(){
    this.isLoading =true;
    this.noDataFound =false;
    this.apiservice.getMethod('/admin/allsentrequest/'+this.loginData._id).subscribe((res:any)=>{

       this.sendData = res.data;
       if( this.sendData &&  this.sendData.length == 0){
        this.noDataFound =true;
       }
       this.isLoading = false;
      }, (err) => {
        this.isLoading = false;
      })
  }


}
