import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';
import { Router, ActivatedRoute, } from "@angular/router";


@Component({
  selector: 'app-premium-page',
  templateUrl: './premium-page.component.html',
  styleUrls: ['./premium-page.component.css']
})
export class PremiumPageComponent implements OnInit {
 goldData:any;
 sapphiredData:any;
 platinumData:any;
 isLoading:boolean;
  constructor(private apiservice :ApiServiceService,private router: Router) { }

  ngOnInit(): void {
    this.getMemberShip();
  }

  gotoHome(){
    this.router.navigate(['dashboard/dashbaord_page']);
  }
  getMemberShip(){
    this.isLoading =true;
    this.apiservice.getMethod("/admin/allmembership").subscribe((data:any)=>{
      let memberData = data.data;
      debugger
      this.goldData = memberData.find((d)=> d.name == "GOLD");
      this.sapphiredData = memberData.find((d)=> d.name == "SAPPHIRE"); 
      this.platinumData = memberData.find((d)=> d.name == "PLATINUM");
      this.isLoading=false;
    },
    (err)=>{
      this.isLoading=false;
    })
  }
}
