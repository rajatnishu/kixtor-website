import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';
import { countryList } from '../../countryList';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {
  loginData:any;
  countryListArray :any = countryList
  userList:any=[];
  country:any;
  kixtorId:String;
  keywords:String;
  constructor(private apiservice :ApiServiceService) { }

  ngOnInit(): void {
    let data = localStorage.getItem("USER");
    if (data) {
      this.loginData = JSON.parse(data).data
    }
  }

  async searchUser(){
  this.userList = [];
    let payload={
      country:this.country.name,
      kixtor_id:this.kixtorId,
      user_id:this.loginData._id
    }
    await this.apiservice.postMethod('/admin/searchuser' ,payload).subscribe((res:any)=>{
      this.userList = res.data;
    })
  }

  sendRequest(receiverId){
   let payload={
     sender_id:this.loginData._id,
     receiver_id:receiverId,
     status:'SEND'
   }

   this.apiservice.postMethod('/admin/sendrequesttouser',payload).subscribe((res:any)=>{
       debugger
       res
   })
  }

}
