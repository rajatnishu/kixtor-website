import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../api-service.service';
@Component({
  selector: 'app-mentorpage',
  templateUrl: './mentorpage.component.html',
  styleUrls: ['./mentorpage.component.css']
})
export class MentorpageComponent implements OnInit {

  constructor(private apiservice:ApiServiceService) { }

  ngOnInit(): void {
    this.getallmentor()
  }

  getmentor:any =[]
  getallmentor() {
    this.apiservice.getMethod('/admin/allmentor').subscribe((data: any) => {
      if (data.status == 200) {
        this.getmentor = data.result
        debugger
      }
    })
  }

}
