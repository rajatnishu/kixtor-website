import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor(private apiService:ApiServiceService) { }
  terms : any

  ngOnInit(): void {

    let data = {
      type: 'About'
    }
    this.apiService.postMethod('/admin/allcms',data).subscribe((response:any)=>{
      this.terms = response.data.content;
    })
  }

}
