import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailboxPendingComponent } from './mailbox-pending.component';

describe('MailboxPendingComponent', () => {
  let component: MailboxPendingComponent;
  let fixture: ComponentFixture<MailboxPendingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailboxPendingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailboxPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
