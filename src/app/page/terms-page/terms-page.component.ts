import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-terms-page',
  templateUrl: './terms-page.component.html',
  styleUrls: ['./terms-page.component.css']
})
export class TermsPageComponent implements OnInit {

  constructor(private apiService:ApiServiceService) { }
  terms : any
  ngOnInit(): void {
    let data = {
      type: 'Terms'
    }
    this.apiService.postMethod('/admin/allcms',data).subscribe((response:any)=>{
      this.terms = response.data.content;
    })
  }

}
