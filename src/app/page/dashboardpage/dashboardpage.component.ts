import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-dashboardpage',
  templateUrl: './dashboardpage.component.html',
  styleUrls: ['./dashboardpage.component.css']
})
export class DashboardpageComponent implements OnInit {
  loginData: any;

  constructor(private router :Router, private apiService:ApiServiceService) { }

  ngOnInit(): void {
    let data = localStorage.getItem("USER");
    if (data) {
      this.loginData = JSON.parse(data).data
    }
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['dashboard/indexpage'])
  }

  gotopage(){
    this.router.navigate(['dashboard/editprofile_page']);
  }
}
