import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexPageComponent } from './page/index-page/index-page.component';
import { LoginpageComponent } from './auth/loginpage/loginpage.component';
import { RegisterpageComponent } from './auth/registerpage/registerpage.component';
import { PasswordpageComponent } from './auth/passwordpage/passwordpage.component';
import { MentorpageComponent } from './page/mentorpage/mentorpage.component';
import { DashboardpageComponent } from './page/dashboardpage/dashboardpage.component';
import { PremiumPageComponent } from './page/premium-page/premium-page.component';
import { SearchPageComponent } from './page/search-page/search-page.component';
import { EditprofilepageComponent } from './page/editprofilepage/editprofilepage.component';
import { MailboxpageComponent } from './page/mailboxpage/mailboxpage.component';
import { MailboxPendingComponent } from './page/mailbox-pending/mailbox-pending.component';
import { TermsPageComponent } from './page/terms-page/terms-page.component';
import { PrivacyComponent } from './page/privacy/privacy.component';
import { AboutUsComponent } from './page/about-us/about-us.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard/indexpage', pathMatch: 'full' },
  { path: 'dashboard/indexpage', component: IndexPageComponent },
  { path: 'auth/login_page', component: LoginpageComponent },
  { path: 'auth/register_page', component: RegisterpageComponent },
  { path: 'auth/forgot_page', component: PasswordpageComponent },
  { path: 'dashboard/mentor_page', component: MentorpageComponent },
  { path: 'dashboard/dashbaord_page', component: DashboardpageComponent },
  { path: 'dashboard/premium_page', component: PremiumPageComponent },
  { path: 'dashboard/search_page', component: SearchPageComponent },
  { path: 'dashboard/editprofile_page', component: EditprofilepageComponent },
  { path: 'dashboard/mailbox_page', component: MailboxpageComponent },
  { path: 'dashboard/mailpending_page', component: MailboxPendingComponent },
  { path: 'dashboard/terms_page', component: TermsPageComponent },
  { path: 'dashboard/privacy_page', component: PrivacyComponent },
  { path: 'dashboard/about_page', component: AboutUsComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
