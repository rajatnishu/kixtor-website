import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'kixtor-website';
  constructor(private router:Router){

  }
  ngOnInit() {
   let userData= JSON.parse(localStorage.getItem("USER"));
   if(userData){
     this.router.navigate(['/dashboard/dashbaord_page']);
   } else {
     this.router.navigate(['/dashboard/indexpage']);
   }
    
  }
}
