import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { IndexPageComponent } from './page/index-page/index-page.component';
import { FooterpageComponent } from './share/footerpage/footerpage.component';
import { HeaderpageComponent } from './share/headerpage/headerpage.component';
import { LoginpageComponent } from './auth/loginpage/loginpage.component';
import { RegisterpageComponent } from './auth/registerpage/registerpage.component';
import { PasswordpageComponent } from './auth/passwordpage/passwordpage.component';
import { MentorpageComponent } from './page/mentorpage/mentorpage.component';
import { DashboardpageComponent } from './page/dashboardpage/dashboardpage.component';
import { FormsModule } from '@angular/forms';
import { PremiumPageComponent } from './page/premium-page/premium-page.component';
import { SearchPageComponent } from './page/search-page/search-page.component';
import { EditprofilepageComponent } from './page/editprofilepage/editprofilepage.component';
import { MailboxpageComponent } from './page/mailboxpage/mailboxpage.component';
import { MailboxPendingComponent } from './page/mailbox-pending/mailbox-pending.component';
import { TermsPageComponent } from './page/terms-page/terms-page.component';
import { PrivacyComponent } from './page/privacy/privacy.component';
import { AboutUsComponent } from './page/about-us/about-us.component';
import { SentRequestComponent } from './page/mailboxpage/template/sent-request/sent-request.component';
import { PendingRequestComponent } from './page/mailboxpage/template/pending-request/pending-request.component';
import { AcceptRequestComponent } from './page/mailboxpage/template/accept-request/accept-request.component';

import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';

const fbLoginOptions = {
  scope: 'pages_messaging,pages_messaging_subscriptions,email,pages_show_list,manage_pages',
  return_scopes: true,
  enable_profile_selector: true
}; // https://developers.facebook.com/docs/reference/javascript/FB.login/v2.11

const googleLoginOptions = {
  scope: 'profile email'
}; // https://developers.google.com/api-client-library/javascript/reference/referencedocs#gapiauth2clientconfig

@NgModule({
  declarations: [
    AppComponent,
    IndexPageComponent,
    FooterpageComponent,
    HeaderpageComponent,
    LoginpageComponent,
    RegisterpageComponent,
    PasswordpageComponent,
    MentorpageComponent,
    DashboardpageComponent,
    PremiumPageComponent,
    SearchPageComponent,
    EditprofilepageComponent,
    MailboxpageComponent,
    MailboxPendingComponent,
    TermsPageComponent,
    PrivacyComponent,
    AboutUsComponent,
    SentRequestComponent,
    PendingRequestComponent,
    AcceptRequestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ToastrModule.forRoot(),
    SocialLoginModule
  ],
  providers: [
    {
      
      provide: 'SocialAuthServiceConfig',
      useValue: {
        
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              "844542863620-q1rlovkfok45u380bsnu1i204kh73jcr.apps.googleusercontent.com",googleLoginOptions)
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('396068754818198',fbLoginOptions)
          }
        ]
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
